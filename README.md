# Levantar el proyecto

1. Instalar [Node.js](https://nodejs.org/es/ "Node.js").

2. Conectarse a la vpn de Galeno y ejecutar `npm install` sobre el origen del proyecto. 
    > En caso de error ejecutar `npm install --force`

3. `npm run storybook`