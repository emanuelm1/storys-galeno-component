import { ComponentMeta, ComponentStory } from "@storybook/react";
import { action } from "@storybook/addon-actions";
import { PrepagaDateTimePicker } from "prepaga-datetime-picker";

export default {
  title: "Componentes/DateTimePicker",
  component: PrepagaDateTimePicker,
} as ComponentMeta<typeof PrepagaDateTimePicker>;

const Template: ComponentStory<typeof PrepagaDateTimePicker> = (args) => (
  <PrepagaDateTimePicker {...args} />
);

export const Basic = Template.bind({});
Basic.args = {
  value: new Date(),
  label: "I am a DateTime Picker",
  onChange: () => {
    action("click");
  },
};

export const Complex = Template.bind({});
Complex.args = {
  value: new Date(),
  label: "I am a better DateTime Picker",
  disabled: false,
  required: false,
  error: false,
  disableToolbar: false,
  format: "dd/MM/yyyy  HH:mm:ss",
  style: { width: "100%" },
  onChange: () => {
    action("click");
  },
  onKeyPress: () => {
    action("click");
  },
};
