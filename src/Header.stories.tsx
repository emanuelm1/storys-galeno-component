import React, { useState } from "react";
import { ComponentMeta, ComponentStory } from "@storybook/react";
import { PrepagaHeader } from "prepaga-header";
import { PrepagaList } from "prepaga-list";
import { PrepagaListItem } from "prepaga-list-item";
import { action } from "@storybook/addon-actions";
import { PrepagaDropdown } from "prepaga-dropdown";
import { PrepagaUserInfo } from "prepaga-user-info";

export default {
  title: "Componentes/Header",
  component: PrepagaHeader,
  parameters: {
    backgrounds: {
      default: "twitter",
      values: [{ name: "twitter", value: "#E5E5E5" }],
    },
  },
} as ComponentMeta<typeof PrepagaHeader>;

const Template: ComponentStory<typeof PrepagaHeader> = (args) => (
  <PrepagaHeader {...args} />
);

export const Basic = Template.bind({});
Basic.args = {
  tooltipText: "I am a tooltip",
  logoSrc: "Image/logo_Galeno.png",
};
export const Complex = Template.bind({});
Complex.args = {
  tooltipText: "I am a better tooltip",
  logoSrc: "Image/logo_Galeno.png",
  rightLinks: (
    <PrepagaUserInfo userSurname="Morales" userName="Nicolás Emanuel" />
  ),
  leftLinks: (
    <PrepagaList>
      <PrepagaListItem>
        <PrepagaDropdown
          left
          buttonText="Casos"
          name="1"
          nameSelected={"selected"}
          dropdownList={[
            "Búsqueda de Casos",
            "Búsqueda de Prestaciones",
            "Habilitar",
          ]}
          onClickItem={() => {
            action("click");
          }}
          onClickMenu={() => {
            action("click");
          }}
        />
      </PrepagaListItem>
      <PrepagaListItem>
        <PrepagaDropdown
          buttonText="Consultas y reportes"
          name="2"
          nameSelected={"2"}
          dropdownList={[]}
        />
      </PrepagaListItem>
      <PrepagaListItem>
        <PrepagaDropdown
          left
          buttonText="Habilitar"
          name="3"
          nameSelected={"selected"}
          dropdownList={[]}
        />
      </PrepagaListItem>
      <PrepagaListItem>
        <PrepagaDropdown
          left
          buttonText="Configuraciones varias"
          name="4"
          nameSelected={"selected"}
          dropdownList={[]}
        />
      </PrepagaListItem>
      <PrepagaListItem>
        <PrepagaDropdown
          left
          buttonText="Vacunas COVID"
          name="5"
          nameSelected={"selected"}
          dropdownList={[]}
        />
      </PrepagaListItem>
      <PrepagaListItem>
        <PrepagaDropdown
          left
          buttonText="Preocupacional"
          name="6"
          nameSelected={"6"}
          dropdownList={[]}
        />
      </PrepagaListItem>
    </PrepagaList>
  ),
};
