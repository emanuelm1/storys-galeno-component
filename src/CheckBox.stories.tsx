import { ComponentMeta, ComponentStory } from "@storybook/react";
import { action } from "@storybook/addon-actions";
import { PrepagaCheckbox } from "prepaga-checkbox";

export default {
  title: "Componentes/CheckBox",
  component: PrepagaCheckbox,
  parameters: {
    layout: "centered",
  },
} as ComponentMeta<typeof PrepagaCheckbox>;

const Template: ComponentStory<typeof PrepagaCheckbox> = (args) => (
  <PrepagaCheckbox {...args} />
);

export const Basic = Template.bind({});
Basic.args = {
  caption: "I am a checkbox",
  checked: true,
  onClick: () => {
    action("click");
  },
};

export const Complex = Template.bind({});
Complex.args = {
  caption: "I am a better checkbox",
  checked: false,
  onClick: action("click"),
  disabled: true,
};
