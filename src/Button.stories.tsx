import { ComponentMeta, ComponentStory } from "@storybook/react";
import { action } from "@storybook/addon-actions";
import { PrepagaButton } from "prepaga-button";

export default {
  title: "Componentes/Button",
  component: PrepagaButton,
  argTypes: {
    color: {
      options: [
        "report-blue",
        "primary-blue",
        "primary-green",
        "back-breadcrumbs",
        "secondary",
      ],
      control: { type: "radio" },
    },
  },
  parameters: {
    layout: "centered",
  },
} as ComponentMeta<typeof PrepagaButton>;

const Template: ComponentStory<typeof PrepagaButton> = (args) => (
  <PrepagaButton {...args} />
);

export const Basic = Template.bind({});
Basic.args = {
  caption: "I am a button",
  color: "primary-blue",
  onClick: () => {
    action("click");
  },
};

export const Complex = Template.bind({});
Complex.args = {
  caption: "I am a better button",
  color: "primary-green",
  onClick: action("click"),
  onChange: action("click"),
  disabled: true,
  className: "",
  width: 350,
};
