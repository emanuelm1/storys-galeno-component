import { ComponentMeta, ComponentStory } from "@storybook/react";
import { action } from "@storybook/addon-actions";
import { PrepagaAutocomplete } from "prepaga-autocomplete";

export default {
  title: "Componentes/Autocomplete",
  component: PrepagaAutocomplete,
} as ComponentMeta<typeof PrepagaAutocomplete>;

const Template: ComponentStory<typeof PrepagaAutocomplete> = (args) => (
  <PrepagaAutocomplete {...args} />
);

export const Basic = Template.bind({});
Basic.args = {
  label: "I am a autocomplete",
  options: [
    "Sanatorio de la Trinidad Ramos Mejía",
    "Sanatorio de la Trinidad Quilmes",
  ],
  style: { width: "200px" },
};

export const Complex = Template.bind({});
Complex.args = {
  label: "I am a better autocompletes",
  options: [
    "Sanatorio de la Trinidad Ramos Mejía",
    "Sanatorio de la Trinidad Quilmes",
  ],
  style: { width: "100%" },
  showClear: false,
  disabled: false,
  required: false,
  error: false,
  onChange: () => {
    action("click");
  },
  onOpen: () => {
    action("click");
  },
  onClose: () => {
    action("click");
  },
  onClear: () => {
    action("click");
  },
  getOptionLabel: (value) => value,
  getOptionSelected: (option, value) => true,
};
